# CI stack Jenkis+Gitlab+Sonarqube with docker
## CNES SonarQube image \[server\]
## Features

This image is based on the official SonarQube LTS image, namely [sonarqube:7.9.4-community](https://hub.docker.com/_/sonarqube), and offers additional features.

Additional features are:

* Mandatory modification of the default admin password to run a container.
* Healthcheck of the container.
* More plugins (see [the list](#sonarqube-plugins-included))
* CNES configuration
    * CNES Java rules
    * CNES Quality Profiles for Java, Python, C and C++
    * CNES Quality Gate (set as default)

_This image is made to be used in conjunction with a pre-configured sonar-scanner image that embeds all necessary tools: [cnescatlab/sonar-scanner](https://github.com/cnescatlab/sonar-scanner). It is, however, not mandatory to use it._

## User guide

This image is available on Docker Hub: [lequal/sonarqube](https://hub.docker.com/r/lequal/sonarqube/).

Since inception, this image has been designed to be used in production. Thus, leaving the default admin password (namely "admin") will never be an option. To this extent, a new password for the admin account shall be given by setting the environment variable `SONARQUBE_ADMIN_PASSWORD`.

:warning: :rotating_light: The container will fail to run if `SONARQUBE_ADMIN_PASSWORD` is empty or equal to "admin".


## SonarQube plugins included

| SonarQube plugin                                  | Version                  | 
|---------------------------------------------------|--------------------------|
| C++ (Community)                                   | 1.3.1 (build 1807)       |
| Checkstyle                                        | 4.21                     |
| Cobertura                                         | 1.9.1                    |
| Community Branch Plugin                           | 1.3.2                    |
| Findbugs                                          | 3.11.0                   |
| Git                                               | 1.8 (build 1574)         |
| GitHub Authentication for SonarQube               | 1.5 (build 870)          |
| JaCoCo                                            | 1.0.2 (build 475)        |
| LDAP                                              | 2.2 (build 608)          |
| PMD                                               | 3.2.1                    |
| Rules Compliance Index (RCI)                      | 1.0.1                    |
| SAML 2.0 Authentication for SonarQube             | 1.2.0 (build 682)        |
| Sonar Frama-C plugin                              | 2.1.1                    |
| Sonar i-Code CNES plugin                          | 2.0.2                    |
| SonarC#                                           | 7.15 (build 8572)        |
| SonarCSS                                          | 1.1.1 (build 1010)       |
| SonarFlex                                         | 2.5.1 (build 1831)       |
| SonarGo                                           | 1.1.1 (build 2000)       |
| SonarHTML                                         | 3.1 (build 1615)         |
| SonarJS                                           | 5.2.1 (build 7778)       |
| SonarJava                                         | 5.13.1 (build 18282)     |
| SonarKotlin                                       | 1.5.0 (build 315)        |
| SonarPHP                                          | 3.2.0.4868               |
| SonarPython                                       | 1.14.1 (build 3143)      |
| SonarQube CNES Export Plugin                      | 1.2                      |
| SonarQube CNES Python Plugin                      | 1.3                      |
| SonarQube CNES Report                             | 3.2.2                    |
| SonarRuby                                         | 1.5.0 (build 315)        |
| SonarScala                                        | 1.5.0 (build 315)        |
| SonarTS                                           | 1.9 (build 3766)         |
| SonarVB                                           | 7.15 (build 8572)        |
| SonarXML                                          | 2.0.1 (build 2020)       |
| Svn                                               | 1.9.0.1295               |

To update this list run:

```sh
while IFS='|' read -r plugin version
do
    printf "| %-.50s| %-.25s|\n" "$plugin                                                  " "$version                         "
done < <(curl -s http://localhost:9000/api/plugins/installed | jq -r '.plugins[] | "\(.name)|\(.version)"')
```
## RUN stack
```sh
git clone https://gitlab.com/Welmaazi/ci-jenkins-sonar-nodjs.git
```
```sh
cd  ci-jenkins-sonar-nodjs && docker-compose up -d 
```
## Test 
Example Nodjs project with sonar-project.properties:
```
https://gitlab.com/Welmaazi/ci-jenkins-sonar-nodjs.git
```
Example jenkins pipline
```
node {
    
    stage('Clone repo') {
         git branch: "master", url: "https://gitlab.com/Welmaazi/backend-noteapp.git" , credentialsId: "" 
    }
    
    stage('SonarTests') {
            sh "docker run -v /home/ubuntu/jenkins_home/workspace/test:/usr/src -w /usr/src  newtmitch/sonar-scanner /usr/local/bin/sonar-scanner -Dsonar.host.url=http:///172.31.31.189:9000   -Dsonar.login=xxxxx "
        }
    }
```


